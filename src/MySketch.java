// Original source code: https://gist.github.com/denkspuren/86e2132b6563d609902e
// Minor adaptations were required for a Java 13 environment.  

import processing.core.PApplet;
import processing.core.PImage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MySketch extends PApplet {
    public static void main(String[] args) {
        String[] appArgs = {"Grab Ball"};
        MySketch mySketch = new MySketch();
        PApplet.runSketch(appArgs, mySketch);
    }

    private static final int WIN_WIDTH = 2736;
    private static final int WIN_HEIGHT = 1824;

    private static final int SNOWFLAKE_RATE = 1000;

    private static PImage star;
    private static PImage tree;
    private static PImage snowflake;


    private  List<Snowflake> snowflakes;
    private List<Star> stars;
    private  Player player;

    private int starsOn;
    private int timer;
    private int counter;


    private int backgroundColor;
    public void settings() {
        size(WIN_WIDTH, WIN_HEIGHT);
    }

    public void setup() {
        star = loadImage("star.png");
        tree = loadImage("tree.png");
        snowflake = loadImage("snowflake.png");
        snowflake.resize(snowflake.width/6,snowflake.height/6);
        tree.resize(tree.width / 2, tree.height / 2);


        starsOn = 0;
        timer = 0;
        counter = 0;

        player = new Player(displayWidth, displayHeight, tree.copy());

        snowflakes = new ArrayList<>();
        stars = new ArrayList<>();
        stars.add(new Star(star.copy(), 1 * displayWidth / 4, 200));
        stars.add(new Star(star.copy(), 2 * displayWidth / 4, 200));
        stars.add(new Star(star.copy(), 3 * displayWidth / 4, 200));
        addSnowFlake();


        backgroundColor = color(22, 43, 100);
        noStroke();
    }


    private int rndColor(){
        return color(random(0,255),random(0,255),random(0,255));
    }



    private void collisionCheck(Star b){


        // collisionCheckWithPlayer
        if (player.getX() < b.getX() + b.getImage().width/2.0f &&
                player.getX() + player.getImage().width > b.getX() &&
                player.getY() < b.getY() + b.getImage().height/2.0f &&
                player.getY() + player.getImage().height > b.getY()) {
            starsOn++;
            b.turnOn();
            player.bounce();
        }
    }

    private void drawStar(Star s){
        image(s.getImage(),s.getX(),s.getY());
    }

    private void drawPlayer(){
        image(player.getImage(),player.getX(),player.getY(),player.getImage().width,player.getImage().height);
    }

    private void drawSnowflake(Snowflake snowflake){
        image(snowflake.getImage(),snowflake.getX(),snowflake.getY());
    }

    public void draw() {
        clear();
        background(backgroundColor);
        if(starsOn < 3) {
            player.updatePos();
            drawPlayer();
            stars.forEach(this::collisionCheck);
            stars.forEach(this::drawStar);

        } else {
            if (timer >= SNOWFLAKE_RATE) {
                addSnowFlake();
                counter++;
                timer = 0;
            }
            snowflakes.forEach(Snowflake::updatePos);
            snowflakes.forEach(this::drawSnowflake);
            timer = millis() - counter * SNOWFLAKE_RATE;
        }
    }

    private void addSnowFlake() {
        snowflakes.add(new Snowflake((int)random(0,displayWidth),-100,snowflake.copy()));
    }


    public void keyPressed(){
        if(keyCode == LEFT) player.moveLeft();
        if(keyCode == RIGHT) player.moveRight();
        if(keyCode == UP) player.moveUp();
        if(keyCode == DOWN) player.moveDown();
    }

}