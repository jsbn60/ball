import processing.core.PApplet;
import processing.core.PApplet.*;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PShape;

public class Snowflake {

    private float x,y,x_vel,y_vel;
    private PImage image;
    public Snowflake(int x, int y, PImage image){
        float size = 3.0f+(float)Math.random()*100f;
        this.x = x;
        this.y = y;
        x_vel = 0;
        y_vel = 2.0f * size/100;
        this.image = image;
        this.image.resize((int)(this.image.width * size / 100),(int)(this.image.height * size / 100));
        //image.filter(PConstants.BLUR,1+(35.0f/(size+1)));
    }


    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }


    public PImage getImage(){
        return image;
    }

    public void updatePos(){
        //x += x_vel;
        y += y_vel;
    }

}
