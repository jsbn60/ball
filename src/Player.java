import processing.core.PImage;

public class Player {

    private static final float START_X_VEL = 25f;
    private static final float START_Y_VEL = 25f;
    private float x, y;
    private float x_vel,y_vel;
    private float drag;
    private float gravity;
    private final int winWidth;
    private final int winHeight;
    private PImage image;

    public Player( int winWidth, int winHeight, PImage image){
        this.winWidth = winWidth;
        this.winHeight = winHeight;
        x = this.winWidth/2.0f;
        y = this.winHeight - image.height;
        x_vel = y_vel = 0;
        drag = 0.95f;
        gravity = 0.5f;
        this.image = image;
    }

    public void bounce(){
        x_vel = -x_vel;
        y_vel = -y_vel;
    }

    public void updatePos(){
        x += x_vel;
        y += y_vel;

        if(x > winWidth) x = 0;
        else if(x < 0) x = winWidth;

        if(y>=winHeight - image.height) {
            y = winHeight - image.height;
            y_vel = 0;
        } else {
            y_vel += gravity;
        }

        if(x_vel != 0 ){
            x_vel *= drag;
        }
    }

    public void moveLeft(){
        x_vel = -START_X_VEL;
    }

    public void moveRight(){
        x_vel = START_X_VEL;
    }

    public void moveUp(){
        y_vel = -START_Y_VEL;
    }

    public void moveDown(){
        y_vel = START_Y_VEL;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public PImage getImage(){
        return image;
    }
}
