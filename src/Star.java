import processing.core.PConstants;
import processing.core.PImage;

public class Star {
    private PImage image;
    private int x;
    private int y;
    private boolean turnedOn;

    public PImage getImage() {
        return image;
    }


    public Star(PImage image, int x, int y){
        this.x = x - image.width/2;
        this.y = y - image.height/2;
        this.image = image;
        turnedOn = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void turnOn(){
        if(!turnedOn){
            image.filter(PConstants.INVERT);
            image.filter(PConstants.BLUR,5);
            turnedOn = true;
        }
    }
}
